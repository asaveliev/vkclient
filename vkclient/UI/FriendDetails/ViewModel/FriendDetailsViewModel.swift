//
//  FriendDetailsViewModel.swift
//  vkclient
//
//  Created by Anton on 04.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import RxSwift
import RxCocoa
import VK_ios_sdk

class FriendDetailsViewModel {
    private let disposeBag = DisposeBag()
    private let service = UserDetailsService()
    private let user = BehaviorRelay<VKUser?>(value: nil)
    private let userId: Int
    
    init(userId: Int) {
        self.userId = userId
    }
    
    func loadData() {
        service.getDetails(by: userId)
            .asDriver(onErrorJustReturn: nil)
            .drive(user)
            .disposed(by: disposeBag)
    }
    
    func bindInformation(_ binder: Binder<VKUser?>) {
        user.bind(to: binder)
            .disposed(by: disposeBag)
    }
}
