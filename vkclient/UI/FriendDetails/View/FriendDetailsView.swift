//
//  FriendDetailsView.swift
//  vkclient
//
//  Created by Anton on 04.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit

class FriendDetailsView: UIView {
    lazy var infoView = FriendInformationView(imageSize: 70)
    lazy var countersView = FriendCountersView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        addSubview(infoView)
        addSubview(countersView)
        
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        infoView.snp.makeConstraints { make in
            make.leading.trailing.top.equalTo(safeAreaLayoutGuide)
        }
        
        countersView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(safeAreaLayoutGuide)
            make.top.equalTo(infoView.snp.bottom)
        }
    }
}
