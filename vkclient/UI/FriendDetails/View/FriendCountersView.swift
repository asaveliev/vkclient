//
//  FriendCountersView.swift
//  vkclient
//
//  Created by Anton on 04.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import VK_ios_sdk

class FriendCountersViewItem: UIView {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = .systemFont(ofSize: 20)
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.font = .systemFont(ofSize: 15)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        
        makeConstraints()
    }
    
    func makeConstraints() {
        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
        }
        subtitleLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(2)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FriendCountersView: UIView {
    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 5
        return stackView
    }()
    
    lazy var friendsCount: FriendCountersViewItem = {
        let item = FriendCountersViewItem()
        item.subtitleLabel.text = "друзей"
        return item
    }()
    
    lazy var commonCount: FriendCountersViewItem = {
        let item = FriendCountersViewItem()
        item.subtitleLabel.text = "общих"
        return item
    }()
    
    lazy var followerCount: FriendCountersViewItem = {
        let item = FriendCountersViewItem()
        item.subtitleLabel.text = "подписчика"
        return item
    }()
    
    lazy var photoCount: FriendCountersViewItem = {
        let item = FriendCountersViewItem()
        item.subtitleLabel.text = "фото"
        return item
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(stackView)
        stackView.addArrangedSubview(friendsCount)
        stackView.addArrangedSubview(commonCount)
        stackView.addArrangedSubview(followerCount)
        stackView.addArrangedSubview(photoCount)
        
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with model: VKUser) {
        friendsCount.titleLabel.text = "\(model.counters.friends.intValue)"
        commonCount.titleLabel.text = "\(model.common_count.intValue)"
        followerCount.titleLabel.text = "\(model.counters.followers.intValue)"
        photoCount.titleLabel.text = "\(model.counters.photos.intValue)"
    }
}

extension Reactive where Base: FriendCountersView {
    var model: Binder<VKUser?> {
        return Binder(self.base) { view, model in
            guard let model = model else { return }
            self.base.configure(with: model)
        }
    }
}
