//
//  FriendDetailsController.swift
//  vkclient
//
//  Created by Anton on 04.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit

class FriendDetailsController: UIViewController {
    let contentView = FriendDetailsView()
    let viewModel: FriendDetailsViewModel
    
    init(userId: Int) {
        viewModel = FriendDetailsViewModel(userId: userId)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Информация"
        
        viewModel.bindInformation(contentView.infoView.rx.model)
        viewModel.bindInformation(contentView.countersView.rx.model)
        viewModel.loadData()
    }
}
