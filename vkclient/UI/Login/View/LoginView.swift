//
//  LoginView.swift
//  vkclient
//
//  Created by Anton on 02.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit
import SnapKit

class LoginView: UIView {
    lazy var loginButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.blue, for: .normal)
        button.setTitle("Login", for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(loginButton)
        backgroundColor = .white
        
        loginButton.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
