//
//  LoginViewController.swift
//  vkclient
//
//  Created by Anton on 02.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    let contentView = LoginView()
    let worker = AuthorizationWorker()
    
    override func loadView() {
        view = contentView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        contentView.loginButton.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
    }
    
    @objc
    func loginAction() {
        worker.authorize { success in
            guard success else { return }
            
            self.navigationController?.pushViewController(FriendsListController(), animated: true)
        }
    }
}
