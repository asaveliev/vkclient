//
//  FriendsListCell.swift
//  vkclient
//
//  Created by Anton on 03.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit
import VK_ios_sdk
import SDWebImage

class FriendsListCell: UITableViewCell {
    lazy var infoView = FriendInformationView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(infoView)
        
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        infoView.snp.makeConstraints { $0.edges.equalToSuperview() }
    }
    
    func configure(with model: VKUser) {
        infoView.configure(with: model)
    }
}
