//
//  FriendInformationView.swift
//  vkclient
//
//  Created by Anton on 04.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import VK_ios_sdk

class FriendInformationView: UIView {
    private let margins = 12
    private let labelsOffset = 2
    private let horizontalOffset = 8
    private let imageSize: CGFloat
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = .systemFont(ofSize: 15)
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = .lightGray
        label.font = .systemFont(ofSize: 12)
        return label
    }()
    
    lazy var avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = imageSize / 2
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    init(frame: CGRect = .zero, imageSize: CGFloat = 30) {
        self.imageSize = imageSize
        
        super.init(frame: frame)
        
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(avatarImageView)
        
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        avatarImageView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview().offset(margins)
            make.size.equalTo(imageSize)
            make.bottom.equalToSuperview().offset(-margins)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(avatarImageView.snp.trailing).offset(horizontalOffset)
            make.trailing.equalToSuperview().offset(-margins)
            make.top.equalToSuperview().offset(margins)
        }
        
        subtitleLabel.snp.makeConstraints { make in
            make.leading.equalTo(avatarImageView.snp.trailing).offset(horizontalOffset)
            make.trailing.equalToSuperview().offset(-margins)
            make.top.equalTo(titleLabel.snp.bottom).offset(labelsOffset)
        }
    }
    
    func configure(with model: VKUser) {
        titleLabel.text = "\(model.first_name ?? "") \(model.last_name ?? "")"
        if let city = model.city {
            subtitleLabel.text = city.title
        } else {
            subtitleLabel.text = nil
        }
        
        avatarImageView.sd_setImage(with: URL(string: model.photo_100), placeholderImage: nil, options: .progressiveDownload, progress: nil, completed: nil)
    }
}

extension Reactive where Base: FriendInformationView {
    var model: Binder<VKUser?> {
        return Binder(self.base) { view, model in
            guard let model = model else { return }
            self.base.configure(with: model)
        }
    }
}
