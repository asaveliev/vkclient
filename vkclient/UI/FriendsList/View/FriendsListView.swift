//
//  FriendsListView.swift
//  vkclient
//
//  Created by Anton on 03.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit

class FriendsListView: UIView {
    lazy var tableView = UITableView()
    
    lazy var refreshControl = UIRefreshControl()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tableView.addSubview(refreshControl)
        tableView.showsVerticalScrollIndicator = false
        addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
