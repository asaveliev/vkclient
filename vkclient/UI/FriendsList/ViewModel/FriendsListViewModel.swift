//
//  FriendsListViewModel.swift
//  vkclient
//
//  Created by Anton on 02.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import RxSwift
import RxCocoa
import VK_ios_sdk

class FriendsListViewModel {
    private let disposeBag = DisposeBag()
    private let provider = FriendsProvider()
    let items = BehaviorRelay<[VKUser]>(value: [])
    
    func refreshData() {
        provider.loadFirstPage()
            .asDriver(onErrorJustReturn: [])
            .drive(items)
            .disposed(by: disposeBag)
    }
    
    func fetchNextPage() {
        provider.loadNextPage()
            .asDriver(onErrorJustReturn: [])
            .map { self.items.value + $0 }
            .drive(items)
            .disposed(by: disposeBag)
    }
}
