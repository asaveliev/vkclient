//
//  FriendsListController.swift
//  vkclient
//
//  Created by Anton on 02.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class FriendsListController: UIViewController {
    private let viewModel = FriendsListViewModel()
    private let contentView = FriendsListView()
    private let disposeBag = DisposeBag()
    
    fileprivate let scrollOffset = 5
    
    private let cellId = "FriendsListCell"
    
    override func loadView() {
        view = contentView
        
        contentView.tableView.register(FriendsListCell.self, forCellReuseIdentifier: cellId)
        
        viewModel.items.share()
            .subscribe(onNext: { _ in
                self.contentView.refreshControl.endRefreshing()
            }, onError: { _ in
                self.contentView.refreshControl.endRefreshing()
            })
            .disposed(by: disposeBag)
        
        viewModel.items
            .bind(to: contentView.tableView.rx.items(cellIdentifier: cellId)) { index, model, cell in
                (cell as? FriendsListCell)?.configure(with: model)
            }
            .disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.refreshData()
        title = "Друзья"
        
        contentView.tableView.delegate = self
        contentView.refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
    }
    
    @objc
    func refreshAction() {
        viewModel.refreshData()
    }
}

extension FriendsListController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == viewModel.items.value.count - scrollOffset else { return }
        viewModel.fetchNextPage()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let userId = viewModel.items.value[indexPath.row].id.intValue
        let viewController = FriendDetailsController(userId: userId)
        navigationController?.pushViewController(viewController, animated: true)
    }
}
