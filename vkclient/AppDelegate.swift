//
//  AppDelegate.swift
//  vkclient
//
//  Created by Anton on 01.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import UIKit
import RxSwift
import VK_ios_sdk

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private let vkAppId = "6621409"
    
    let disposeBag = DisposeBag()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        VKSdk.initialize(withAppId: vkAppId)
        
        let worker = AuthorizationWorker()
        worker.checkAuth().subscribe(onNext: {
            self.setupUI(authorized: $0)
        }).disposed(by: disposeBag)
        
        return true
    }
    
    private func setupUI(authorized hasAuth: Bool) {
        let initialController = hasAuth ? FriendsListController() : LoginViewController()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: initialController)
        window?.makeKeyAndVisible()
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        VKSdk.processOpen(url, fromApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String)
        return true
    }
    
}

