//
//  UserDetailsService.swift
//  vkclient
//
//  Created by Anton on 04.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import VK_ios_sdk
import RxSwift

class UserDetailsService {
    private let vkSdkInstance = VKSdk.instance()
    
    func getDetails(by id: Int) -> Observable<VKUser?> {
        let parameters: [AnyHashable: Any] = [
            "user_ids": id,
            "fields": "photo_100,city,common_count,counters"
        ]
        let request = VKApi.users().get(parameters)
        
        return Observable.create { observer -> Disposable in
            request?.execute(resultBlock: { response in
                guard let modelsArray = response?.parsedModel as? VKUsersArray else {
                    observer.onError(RxError.unknown)
                    return
                }
                
                observer.onNext(modelsArray.firstObject())
                observer.onCompleted()
            }, errorBlock: { error in
                guard let error = error else {
                    observer.onError(RxError.unknown)
                    return
                }
                observer.onError(error)
            })
            
            return Disposables.create()
        }
    }
}
