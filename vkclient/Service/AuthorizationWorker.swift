//
//  AuthorizationWorker.swift
//  vkclient
//
//  Created by Anton on 01.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//
//  Воркер, инкапсулирующий логику работы с OAuth вк
//  не сервис ибо является еще и делегатом а значит содержит кастомную логику
//

import VK_ios_sdk
import RxSwift

class AuthorizationWorker: NSObject {
    private let authScope = ["friends"]
    private var authCompletion: ((Bool) -> Void)?
    
    func checkAuth() -> Observable<Bool> {
        return Observable.create { observer in
            VKSdk.wakeUpSession(self.authScope) { state, error in
                if let error = error {
                    observer.onError(error)
                    return
                }
                
                observer.onNext(state == .authorized)
            }
            
            return Disposables.create()
        }
    }
    
    /// не обернуто в обсервабл, так как написать прокcю над делегатом вк невозможно ибо он кривой и архитектура сдк такая себе
    /// пришлось бы хранить обсервер а это плохо, лучше хранить замыкание
    func authorize(completion: @escaping (Bool) -> Void) {
        VKSdk.instance().register(self)
        VKSdk.instance().uiDelegate = self
        authCompletion = completion
        VKSdk.authorize(authScope)
    }
}

extension AuthorizationWorker: VKSdkDelegate {
    func vkSdkUserAuthorizationFailed() {
        authCompletion?(false)
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        authCompletion?(result.error == nil && result.token != nil)
    }
}

extension AuthorizationWorker: VKSdkUIDelegate {
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let navigationController = appDelegate.window?.rootViewController
        else { return }
        
        navigationController.present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        
    }
}
