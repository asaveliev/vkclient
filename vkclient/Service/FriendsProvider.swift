//
//  FriendsProvider.swift
//  vkclient
//
//  Created by Anton on 02.07.2018.
//  Copyright © 2018 Antoxa. All rights reserved.
//

import RxSwift
import VK_ios_sdk

struct PaginationInfo {
    let count = 20
    var maximumCount: Int
    let offset: Int
    
    var nextPage: PaginationInfo? {
        guard offset <= maximumCount else { return nil }
        return PaginationInfo(maximumCount: maximumCount, offset: offset + count)
    }
}

class FriendsProvider {
    private var paginationInfo = PaginationInfo(maximumCount: 0, offset: 0)
    private let vkSdkInstance = VKSdk.instance()
    
    func loadFirstPage() -> Observable<[VKUser]> {
        paginationInfo = PaginationInfo(maximumCount: 0, offset: 0)
        return getFriends()
    }
    
    func loadNextPage() -> Observable<[VKUser]> {
        guard let paginationInfo = paginationInfo.nextPage else { return Observable.empty() }
        self.paginationInfo = paginationInfo
        return getFriends()
    }
    
    private func getFriends() -> Observable<[VKUser]> {
        let parameters: [AnyHashable: Any] = [
            "count": paginationInfo.count,
            "offset": paginationInfo.offset,
            "fields": "city,photo_100"
        ]
        let request = VKApi.friends().get(parameters)
        
        return Observable.create { observer -> Disposable in
            request?.execute(resultBlock: { response in
                guard
                    let usersArray = response?.parsedModel as? VKUsersArray,
                    let items = usersArray.items as? [VKUser]
                else {
                    observer.onNext([])
                    observer.onCompleted()
                    return
                }
                
                self.paginationInfo.maximumCount = Int(usersArray.count)
                
                observer.onNext(items)
                observer.onCompleted()
            }, errorBlock: { error in
                guard let error = error else {
                    observer.onError(RxError.unknown)
                    return
                }
                observer.onError(error)
            })
            
            return Disposables.create()
        }
    }
}
